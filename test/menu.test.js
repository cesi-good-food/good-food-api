import mongoose from 'mongoose';
import { app } from '../src/app';
import supertest from 'supertest';

const DB_URI = process.env.DB_URI || 'mongodb://127.0.0.1';
const databaseName = 'test';
const request = supertest(app);

const menu = {
  name: `Mille feuille pistache chocolat`,
  description: 'Desert',
  price: 5.2,
};

let response;

describe('menus-test', () => {
  beforeAll(async () => {
    const url = `${DB_URI}/${databaseName}`;
    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log(DB_URI);
  });

  afterAll(() => {
    mongoose.connection.close();
  });

  it('/POST Menu', (done) => {
    request
      .post('/menus')
      .send(menu)
      .expect(200)
      .expect((res) => {
        expect(res.body.name).toBe(menu.name);
        expect(res.body.description).toBe(menu.description);
        expect(res.body.price).toBe(menu.price);
      })
      .end((err, res) => {
        response = res;
        if (err) done(err);
        return done();
      });
  });

  it('/GET Menu', (done) => {
    console.log(`/menus/${response.body._id}`);
    request
      .get(`/menus/${response.body._id}`)
      .send(menu)
      .expect(200)
      .expect((res) => {
        console.log(res.body);
        expect(res.body.name).toBe(menu.name);
        expect(res.body.description).toBe(menu.description);
        expect(res.body.price).toBe(menu.price);
      })
      .end((err, res) => {
        response = res;
        if (err) done(err);
        return done();
      });
  });

  it('/GET Menus', (done) => {
    request
      .get('/menus')
      .expect(200)
      .end((err) => {
        if (err) done(err);
        return done();
      });
  });
});
