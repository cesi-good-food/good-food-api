import supertest from 'supertest';
import { app } from '../src/app';

const request = supertest(app);

describe('Test the root path', () => {
  test('It should response the GET method', async () => {
    const response = await request.get('/check');
    expect(response.statusCode).toBe(200);
  });
});
