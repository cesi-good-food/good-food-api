import cors from 'cors'
import express from 'express'
import { clientRouter } from './client/client.route'
import { menuRouter } from './menus/menu.route'
import { restaurantRouter } from './restaurant/restaurant.route'
import { handleError, handleNotFound } from './shared/error'

const app = express()

app.use(cors())
app.use(express.json())

app.use('/restaurants', restaurantRouter)
app.use('/menus', menuRouter)
app.use('/clients', clientRouter)
app.get('/check', (req, res) => {
  res.status(200).json('Alive !')
})

app.use(handleNotFound)
app.use(handleError)

export { app }
