import { Menu } from './menu.model';
import { ErrorHandler } from '../shared/error';

export const getMenus = async (req, res, next) => {
  try {
    const result = await Menu.find();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export const getMenu = async (req, res, next) => {
  const { id } = req.params;
  try {
    const result = await Menu.findById(id);
    if (Array.isArray(result) && result.length === 0) {
      throw new ErrorHandler(404, `Menu with id: ${id} not found`);
    }
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export const createMenu = async (req, res, next) => {
  const { body } = req;
  const restaurant = new Menu({ ...body });
  try {
    const result = await restaurant.save();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export const updateMenu = async (req, res, next) => {
  const { id } = req.params;
  const { body } = req;
  console.log(body);
  try {
    const result = await Menu.findByIdAndUpdate(id, { ...body });
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export const deleteMenus = async (req, res, next) => {
  try {
    await Menu.deleteMany();
    res.status(200).json('All menu have been deleted');
  } catch (error) {
    next(error);
  }
};

export const deleteMenu = async (req, res, next) => {
  const { id } = req?.params;
  try {
    const result = await Menu.findByIdAndDelete(id);
    if (Array.isArray(result) && result.length === 0) {
      throw new ErrorHandler(404, `Menu with id: ${id} not found`);
    }
    await Menu.deleteOne({ id: id });
    res.status(200).json(`Menu with ${id} has been deleted`);
  } catch (error) {
    next(error);
  }
};
