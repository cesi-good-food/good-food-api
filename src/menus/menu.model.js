import Mongoose, { Schema } from 'mongoose';

export const Menu = Mongoose.model(
  'Menu',
  new Schema(
    {
      name: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      restaurants: [
        {
          type: Mongoose.Schema.Types.ObjectId,
          ref: 'Restaurant',
        },
      ],
    },
    { timestamps: true }
  )
);
