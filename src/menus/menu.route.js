import express from 'express';
import {
  getMenu,
  getMenus,
  createMenu,
  updateMenu,
  deleteMenu,
  deleteMenus,
} from './menu.controler';

const menuRouter = express.Router();

menuRouter.get('/', getMenus);
menuRouter.get('/:id', getMenu);
menuRouter.post('/', createMenu);
menuRouter.post('/:id', updateMenu);
menuRouter.delete('/', deleteMenu);
menuRouter.delete('/:id', deleteMenus);

export { menuRouter };
