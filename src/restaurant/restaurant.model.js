import Mongoose from 'mongoose'

const Schema = Mongoose.Schema

const restaurantSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'name is required'],
    },
    address: {
      type: String,
    },
    phoneNumber: {
      type: String,
      required: [true, 'phone number is required'],
    },
    img: {
      type: String,
    },
    menus: [
      {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Menu',
      },
    ],
  },
  { timestamps: true }
)

export const Restaurant = Mongoose.model('Restaurant', restaurantSchema)
