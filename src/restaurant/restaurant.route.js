import express from 'express';
import {
  getRestaurants,
  getRestaurantWithMenus,
  getRestaurant,
  createRestaurant,
  updateRestaurant,
  addMenusToRestaurant,
  removeMenusToRestaurant,
  deleteRestaurant,
  deleteRestaurants,
} from './restaurant.controler';

const restaurantRouter = express.Router();

restaurantRouter.get('/', getRestaurants);
restaurantRouter.get('/:id/withMenus', getRestaurantWithMenus);
restaurantRouter.get('/:id', getRestaurant);
restaurantRouter.post('/', createRestaurant);
restaurantRouter.post('/:id', updateRestaurant);
restaurantRouter.post('/:id/addMenus', addMenusToRestaurant);
restaurantRouter.post('/:id/removeMenus', removeMenusToRestaurant);
restaurantRouter.delete('/', deleteRestaurants);
restaurantRouter.delete('/:id', deleteRestaurant);

export { restaurantRouter };
