import 'regenerator-runtime/runtime.js'

import mongoose from 'mongoose'

import { app } from './app'

const DB_URI = process.env.DB_URI
const PORT = process.env.PORT

mongoose.connect(DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
mongoose.connection.on('connected', () => {
  console.log('Mongoose is connected')
})

app.listen(PORT, () => {
  console.log(`Server running : ${new Date()} at port ${PORT}`)
})
